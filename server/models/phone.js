'use strict'
module.exports = (sequelize, DataTypes) => {
  var Phone = sequelize.define(
    'Phone',
    {
      phone_numbers: {
        type: DataTypes.STRING,
        allowNull: false
      }
    }
  )
  Phone.associate = models => {
    Phone.belongsTo(models.Phone, {
      forignKey: 'contactId',
      onDelete: 'CASCADE'
    })
  }
  return Phone
}
