module.exports = (sequelize, DataTypes) => {
  var Contact = sequelize.define(
    'Contact',
    {
      first_name: {
        type: DataTypes.STRING,
        allowNull: false
      },
      last_name: {
        type: DataTypes.STRING,
        allowNull: false
      },
      phone_numbers: {
        type: DataTypes.ARRAY(DataTypes.STRING),
        allowNull: false
      }
    }
  )
  Contact.associate = models => {
    // Contact.hasMany(models.Phone, {
    //   foreignKey: 'contactId',
    //   as: 'phones'
    // })
  }
  return Contact
}
