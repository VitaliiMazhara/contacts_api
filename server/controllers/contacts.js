const Contact = require('../models').Contact
const Phone = require('../models').Phone
const sortByKey = require('../utils').sortByKey

module.exports = {
  create(req, res) {
    return Contact.create({
      first_name: req.body.first_name,
      last_name: req.body.last_name,
      phone_numbers: req.body.phone_numbers
    })
      .then(item => {
        return res.status(201).send(item)
      })
      .catch(error => {
        return res.status(400).send(error)
      })
  },
  list(req, res) {
    return Contact.findAll({
      attributes: ['id', 'first_name', 'last_name', 'phone_numbers']
    })
      .then(items => {
        let contactsSorted = sortByKey(items, 'last_name')
        return res.status(200).send(contactsSorted)
      })
      .catch(error => res.status(400).send(error))
  },
  retrieve(req, res) {
    return Contact.findById(req.params.contactId)
      .then(item => {
        if (!item) {
          return res.status(404).send({
            message: 'Contact not Found'
          })
        }
        return res.status(200).send(item)
      })
      .catch(error => res.status(400).send(error))
  },
  update(req, res) {
    return Contact.findById(req.params.contactId)
      .then(item => {
        if (!item) {
          return res.status(404).send({
            message: 'Contact not Found'
          })
        }
        return item
          .update({
            first_name: req.body.first_name,
            last_name: req.body.last_name,
            phone_numbers: req.body.phone_numbers
          })
          .then(itemUpd => res.status(200).send(itemUpd))
          .catch(error => res.status(400).send(error))
      })
      .catch(error => res.status(400).send(error))
  },
  destroy(req, res) {
    return Contact.findById(req.params.contactId)
      .then(item => {
        if (!item) {
          return res.status(400).send({
            message: 'Contact Not Found'
          })
        }

        return item
          .destroy()
          .then(() => res.status(204).send())
          .catch(error => res.status(400).send(error))
      })
      .catch(error => res.status(400).send(error))
  }
}
