const contactsController = require('../controllers').contacts

module.exports = app => {
  app.get('/api', (req, res) =>
    res.status(200).send({
      message: 'HI ON API'
    })
  )

  app.post('/api/contacts', contactsController.create)
  app.get('/api/contacts', contactsController.list)
  app.get('/api/contacts/:contactId', contactsController.retrieve)
  app.put('/api/contacts/:contactId', contactsController.update)
  app.delete('/api/contacts/:contactId', contactsController.destroy)
}
